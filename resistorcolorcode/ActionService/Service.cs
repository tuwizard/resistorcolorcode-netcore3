﻿using BusinessDataObject;
using System;
using System.Collections.Generic;

namespace ActionService
{
    // impletement of IService interface. It can handle different data providers
    // ** Facade pattern
    // ** Repository pattern (Service could be split up in individual repositories for Resistor ColorCode, Capacitor Color Code)
    public class Service : IService
    {
        private readonly ColorCodeDAO colorCodeDao = new ColorCodeDAO();

        /// <summary>
        /// Calculate the Ohm value of a resistor based on the band color.
        /// </summary>
        /// <param name="bandAColor">The color of the first figure of component value band.</param>
        /// <param name="bandBColor">The color of the second significant figure band.</param>
        /// <param name="bandCColor">The color of the decimal multiplier band.</param>
        /// <param name="bandDColor">The color of the tolerance value band.</param>
        /// <param name="isMinValue">check min value or not</param>
        /// <param name="baseValue">base value</param>
        /// <returns></returns>
        public int CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor, bool isMinValue = false, bool baseValue = true)
        {
            double retOhmValue = 0;

            ColorCode bandAcolorCode = colorCodeDao.GetColorCodeByColor(bandAColor);
            ColorCode bandBcolorCode = colorCodeDao.GetColorCodeByColor(bandBColor);
            ColorCode bandCcolorCode = colorCodeDao.GetColorCodeByColor(bandCColor);
            ColorCode bandDcolorCode = colorCodeDao.GetColorCodeByColor(bandDColor);

            string ohmValue = bandAcolorCode.Digit.ToString() + bandBcolorCode.Digit.ToString();
            if (!String.IsNullOrEmpty(ohmValue))
            {
                retOhmValue = Convert.ToInt32(ohmValue);
                if (!String.IsNullOrEmpty(bandCcolorCode.Multiplier.ToString()))
                {
                    retOhmValue *= bandCcolorCode.Multiplier;
                }

                if (bandDcolorCode.Tolerance > 0 && !baseValue)
                {
                    if (isMinValue)
                    {
                        retOhmValue -= ((retOhmValue * bandDcolorCode.Tolerance) / 100);
                    }
                    else
                    {
                        retOhmValue += ((retOhmValue * bandDcolorCode.Tolerance) / 100);
                    }
                }
            }
            return Convert.ToInt32(retOhmValue);
        }

        public List<ColorCode> GetColorCodeList()
        {
            return colorCodeDao.GetColorCodes();
        }

    }
}

