using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessDataObject;

namespace ResistorColorCodeUnitTest
{
    [TestClass]
    public class ResistorColorCodeUnitTest
    {
        [TestMethod]
        public void GetColorCodeDataFromDBorFile()
        {
            // Arrange
            ColorCodeDAO ccDao = new ColorCodeDAO();

            // Act
            List<ColorCode> listCC = ccDao.GetColorCodes();

            // Assert
            Assert.IsTrue(listCC.Count > 0, "Confirm! there is data return from database.");
        }

        [TestMethod]
        public void GetSpecificColorCode()
        {
            // Arrange
            ColorCodeDAO ccDao = new ColorCodeDAO();

            // Act
            ColorCode cc = ccDao.GetColorCodeByColor("Yellow");

            // Assert
            Assert.IsTrue(cc.Digit > 0, "Digit greater than zero");
        }
    }
}
