﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessDataObject;
using ActionService;

namespace ResistorColorCodeUnitTest
{
    [TestClass]
    public class ServiceUnitTest
    {
        [TestMethod]
        public void GetMaxOhmValue()
        {
            // Arrange
            Service service = new Service();

            // Act
            int ohmValue = service.CalculateOhmValue("Yellow", "Violet", "Red", "Gold", false, false);

            // Assert
            Assert.IsTrue(ohmValue > 0, "Ohm Max value is valid");
        }

        [TestMethod]
        public void GetMinOhmValue()
        {
            // Arrange
            Service service = new Service();

            // Act
            int ohmMinValue = service.CalculateOhmValue("Yellow", "Violet", "Red", "Gold", true, false);

            // Assert
            Assert.IsTrue(ohmMinValue > 0, "Ohm Min value is valid");
        }

        [TestMethod]
        public void GetColorCodeListValues()
        {
            // Arrange
            Service service = new Service();

            // Act
            List<ColorCode> listCC = service.GetColorCodeList();

            // Assert
            Assert.IsTrue(listCC.Count > 0, "Confirm! there is data return from BusinessData Layer.");
        }
    }
}
