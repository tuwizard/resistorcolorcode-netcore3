﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessDataObject
{
    public class ColorCodeDAO : IColorCodeDAO
    {
        readonly DbHelper db = new DbHelper();

        public List<ColorCode> GetColorCodes()
        {
            List<ColorCode> colorCodeList = db.GetColorCodesData();
            return colorCodeList;
        }

        public ColorCode GetColorCode(int colorCodeId)
        {
            ColorCode colorCode = new ColorCode();
            return colorCode;
        }

        public ColorCode GetColorCodeByColor(string bandColor)
        {
            ColorCode colorCode = new ColorCode();
            List<ColorCode> colorCodes = GetColorCodes();

            colorCode = colorCodes.Where(cc => cc.Color == bandColor).FirstOrDefault();

            return colorCode;
        }
    }
}
