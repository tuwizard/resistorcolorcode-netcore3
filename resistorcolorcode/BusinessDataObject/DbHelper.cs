﻿using Newtonsoft;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessDataObject
{
    public class DbHelper
    {
        public List<ColorCode> GetColorCodesData()
        {
            string colorCodeJsonData = "[ {   \"Id\": 1,   \"Color\": \"Black\",   \"Digit\": 0,   \"Multiplier\": 1,   \"Tolerance\": 0 }, {   \"Id\": 2,   \"Color\": \"Brown\",   \"Digit\": 1,   \"Multiplier\": 10,   \"Tolerance\": 1 }, {   \"Id\": 3,   \"Color\": \"Red\",   \"Digit\": 2,   \"Multiplier\": 100,   \"Tolerance\": 2 }, {   \"Id\": 4,   \"Color\": \"Orange\",   \"Digit\": 3,   \"Multiplier\": 1000,   \"Tolerance\": 0 }, {   \"Id\": 5,   \"Color\": \"Yellow\",   \"Digit\": 4,   \"Multiplier\": 10000,   \"Tolerance\": 0 }, {   \"Id\": 6,   \"Color\": \"Green\",   \"Digit\": 5,   \"Multiplier\": 100000,   \"Tolerance\": 0.5 }, {   \"Id\": 7,   \"Color\": \"Blue\",   \"Digit\": 6,   \"Multiplier\": 1000000,   \"Tolerance\": 0.25 }, {   \"Id\": 8,   \"Color\": \"Violet\",   \"Digit\": 7,   \"Multiplier\": 10000000,   \"Tolerance\": 0.1 }, {   \"Id\": 9,   \"Color\": \"Grey\",   \"Digit\": 8,   \"Multiplier\": 100000000,   \"Tolerance\": 0 }, {   \"Id\": 10,   \"Color\": \"White\",   \"Digit\": 9,   \"Multiplier\": 1000000000,   \"Tolerance\": 0 }, {   \"Id\": 11,   \"Color\": \"Gold\",   \"Digit\": 0,   \"Multiplier\": 0.1,   \"Tolerance\": 5 }, {   \"Id\": 12,   \"Color\": \"Silver\",   \"Digit\": 0,   \"Multiplier\": 0.01,   \"Tolerance\": 10 }, {   \"Id\": 13,   \"Color\": \"Pink\",   \"Digit\": 0,   \"Multiplier\": 0.001,   \"Tolerance\": 0 }, {   \"Id\": 14,   \"Color\": \"None\",   \"Digit\": 0,   \"Multiplier\": 0,   \"Tolerance\": 20 }]";
            List<ColorCode> retObj = JsonConvert.DeserializeObject<List<ColorCode>>(colorCodeJsonData); // Convert fake Json data to .NET/CORE list of color code object
            return retObj;
        }
    }
}
