﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessDataObject
{
    // ColorCode business object
    // ** Enterprise Design Pattern: Domain Model, Identity Field
    public class ColorCode
    {
        // * Enterprise Design Pattern: Identity field pattern
        public int Id { get; set; }
        public string Color { get; set; }
        public int Digit { get; set; }
        public double Multiplier { get; set; }
        public double Tolerance { get; set; }
    }
}
