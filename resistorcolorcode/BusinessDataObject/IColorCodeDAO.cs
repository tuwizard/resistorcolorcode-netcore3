﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessDataObject
{
    // Defines DB independent methods to access ColorCode data table/file
    // ** DAO Data Access Object pattern
    public interface IColorCodeDAO
    {
        // Get a list of ColorCode object (repositories)
        List<ColorCode> GetColorCodes();

        // Get a specific ColorCode by Id
        ColorCode GetColorCode(int colorCodeId);

        // Get a specific ColorCode by band color
        ColorCode GetColorCodeByColor(string bandColor);
    }
}
