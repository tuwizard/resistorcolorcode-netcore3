﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Frontend_MVCApp.Models;
using BusinessDataObject;
using ActionService;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Frontend_MVCApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IService _service;
        private readonly ILogger<HomeController> _logger;

        // ** Constructor Dependency Injection (DI).
        public HomeController(ILogger<HomeController> logger, IService service)
        {
            _logger = logger;
            _service = service;
        }

        public ActionResult ResistorColorCode(string bandAColor, string bandBColor, string bandCColor, string bandDColor)
        {
            ViewBag.Message = "Resistor Color Code";

            if (string.IsNullOrEmpty(bandAColor) && string.IsNullOrEmpty(bandBColor) && string.IsNullOrEmpty(bandCColor) && string.IsNullOrEmpty(bandDColor))
            {
                bandAColor = "Black";
                bandBColor = "Brown";
                bandCColor = "Red";
                bandDColor = "Orange";
            }

            var model = new ColorCodesViewModel();
            var colorCodes = _service.GetColorCodeList();
            var mapperConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<ColorCode, ColorCodeViewModel>();
                cfg.CreateMap<ColorCodeViewModel, ColorCode>();
            });

            IMapper mapper = mapperConfig.CreateMapper();
            var colorCodeModels = mapper.Map<List<ColorCode>, List<ColorCodeViewModel>>(colorCodes);

            model.ColorCodes = new SelectList((from c in colorCodeModels.ToList()
                                               select new
                                               {
                                                   c.Color,
                                                   ID_Color = c.Id + "-" + c.Color
                                               })
            , "Color"
            , "Id_Color"
            , null
            );

            model.MultiplierColorCodes = new SelectList((from c in colorCodeModels.Where(c => c.Multiplier != 0).ToList()
                                                         select new
                                                         {
                                                             c.Color,
                                                             ID_Color = c.Color + " x" + c.Multiplier
                                                         })
            , "Color"
            , "Id_Color"
            , null
            );

            model.ToleranceColorCodes = new SelectList((from c in colorCodeModels.Where(c => c.Tolerance != 0).ToList()
                                                        select new
                                                        {
                                                            c.Color,
                                                            ID_Color = c.Color + " " + c.Tolerance + "%"
                                                        })
            , "Color"
            , "Id_Color"
            , null
            );

            if (!string.IsNullOrEmpty(bandAColor) && !string.IsNullOrEmpty(bandBColor) && !string.IsNullOrEmpty(bandCColor) && !string.IsNullOrEmpty(bandDColor))
            {
                ViewBag.BaseValue = _service.CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor, false, true);
                ViewBag.Tolerance = bandDColor;
                ViewBag.MinValue = _service.CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor, true, false);
                ViewBag.MaxValue = _service.CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor, false, false);
            }

            return View(model);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
