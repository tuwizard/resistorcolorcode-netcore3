﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Frontend_MVCApp.Models
{
    // list of ColorCodes ViewModel

    // ** Data Transfer Object (DTO) pattern
    public class ColorCodesViewModel
    {
        public SelectList ColorCodes { get; set; }
        public SelectList MultiplierColorCodes { get; set; }
        public SelectList ToleranceColorCodes { get; set; }
    }
}
