﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    // For DTO Output resistor color code
    public class ColorCodesViewModel
    {
        public int BaseValue { get; set; }
        public string Tolerance { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
    }
}
