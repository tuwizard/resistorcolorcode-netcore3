﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    // ColorCode ViewModel

    // ** Data Transfer Object (DTO) pattern
    public class ColorCodeViewModel
    {
        public int Id { get; set; }
        public string Color { get; set; }
        public int Digit { get; set; }
        public double Multiplier { get; set; }
        public double Tolerance { get; set; }
    }
}
