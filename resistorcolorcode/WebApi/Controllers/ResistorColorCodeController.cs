﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ActionService;
using BusinessDataObject;
using WebApi.Models;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResistorColorCodeController : ControllerBase
    {
        private readonly IService _service;
        // Constructor: DI pattern
        public ResistorColorCodeController(IService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("GetRCCList", Name = "GetColorCodeList")]
        public IEnumerable<ColorCode> GetColorCodeList()
        {
            List<ColorCode> colorCodeList = _service.GetColorCodeList();

            return colorCodeList;
        }

        [HttpGet]
        [Route("GetSelectedRCC", Name = "GetSelectedResistorColorCode")]
        public ColorCodesViewModel GetSelectedResistorColorCode(string bandAColor, string bandBColor, string bandCColor, string bandDColor)
        {
            if (string.IsNullOrEmpty(bandAColor) && string.IsNullOrEmpty(bandBColor) && string.IsNullOrEmpty(bandCColor) && string.IsNullOrEmpty(bandDColor))
            {
                bandAColor = "Black"; // Default values
                bandBColor = "Black";
                bandCColor = "Black";
                bandDColor = "Black";
            }

            var model = new ColorCodesViewModel();
            var colorCodes = _service.GetColorCodeList();
            var mapperConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<ColorCode, ColorCodeViewModel>();
                cfg.CreateMap<ColorCodeViewModel, ColorCode>();
            });

            IMapper mapper = mapperConfig.CreateMapper();
            var colorCodeModels = mapper.Map<List<ColorCode>, List<ColorCodeViewModel>>(colorCodes);

            if (!string.IsNullOrEmpty(bandAColor) && !string.IsNullOrEmpty(bandBColor) && !string.IsNullOrEmpty(bandCColor) && !string.IsNullOrEmpty(bandDColor))
            {
                var baseValue = _service.CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor, false, true);
                var tolerance = bandDColor;
                var minValue = _service.CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor, true, false);
                var maxValue = _service.CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor, false, false);
                model.BaseValue = baseValue;
                model.Tolerance = tolerance;
                model.MinValue = minValue;
                model.MaxValue = maxValue;
            }

            return model;
        }


        // GET: api/<ResistorColorCodeController>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<ResistorColorCodeController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<ResistorColorCodeController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<ResistorColorCodeController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ResistorColorCodeController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
